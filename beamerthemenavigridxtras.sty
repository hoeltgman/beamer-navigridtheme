% -*- fill-column: 100; comment-column: 81; -*-
%
% Copyright 2014-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
% http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{beamerthemenavigridxtras}[2018/09/01 v0.2 Navigrid beamer theme addons]

\RequirePackage{etoolbox}
\RequirePackage{xargs}
\RequirePackage{ifthen}

\newcounter{linkcounter}
\setcounter{linkcounter}{1}

\@ifpackageloaded{algorithm2e}{%
  \setlength{\algoheightrule}{\heavyrulewidth}
  \setlength{\algotitleheightrule}{\lightrulewidth}
}{%
  % If not loaded
}

\@ifpackageloaded{listings}{%
  \lstset{
    backgroundcolor=\color{lstbg},
    basicstyle=\ttfamily\normalsize,
    breakatwhitespace=true,
    breaklines=true,
    captionpos=t,
    commentstyle=\color{lstco},
    frame=lines,
    keepspaces=true,
    keywordstyle=\color{lstkw}\bfseries,
    language=Matlab,
    xleftmargin=0.3cm,
    framesep=8pt,
    deletekeywords={%
      type
    },
    morekeywords={%
      break,%
      case,%
      catch,%
      classdef,%
      continue,%
      else,%
      elseif,%
      end,%
      for,%
      function,%
      global,%
      if,%
      otherwise,%
      parfor,%
      persistent,%
      return,%
      spmd,%
      switch,%
      try,%
      while,%
      properties,%
      methods,%
      ones,%
      zeros,%
      nan,%
      inf,%
      true,%
      false,%
      numel,%
      persistent,%
      strcat,%
      subsref,%
      subsasgn,%
      subsindex,%
      double,%
      single,%
      int8,%
      int16,%
      int32,%
      int64,%
      uint8,%
      uint16,%
      uint32,%
      uin64,%
      char%
    },
    numbers=none,
    numbersep=5pt,
    numberstyle=\color{lstlnl},
    rulecolor=\color{black},
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    stepnumber=2,
    stringstyle=\color{lststr},
    framerule=\heavyrulewidth,
    tabsize=4
  }
  \renewcommand\lstlistingname{Code}
}{%
  % If not loaded
}

\@ifpackageloaded{biblatex}{%
  \setbeamertemplate{bibliography item}{%
    \ifboolexpr{ test {\ifentrytype{book}} or test {\ifentrytype{mvbook}}
      or test {\ifentrytype{collection}} or test {\ifentrytype{mvcollection}}
      or test {\ifentrytype{reference}} or test {\ifentrytype{mvreference}} }
    {\setbeamertemplate{bibliography item}[book]}
    {\ifentrytype{online}
      {\setbeamertemplate{bibliography item}[online]}
      {\setbeamertemplate{bibliography item}[article]}}%
    \usebeamertemplate{bibliography item}}
  %
  \defbibenvironment{bibliography}
  {\list{}
    {\settowidth{\labelwidth}{\usebeamertemplate{bibliography item}}%
      \setlength{\leftmargin}{\labelwidth}%
      \setlength{\labelsep}{\biblabelsep}%
      \addtolength{\leftmargin}{\labelsep}%
      \setlength{\itemsep}{\bibitemsep}%
      \setlength{\parsep}{\bibparsep}}}
  {\endlist}
  {\item}
}{%
  % If not loaded
}%

%
% -- Create macros to hyperlink images to zoomed version at the end ------------------------------ %
% Enable zooming of pictures by clicking on it.
% SOURCE:
% http://tfischernet.wordpress.com/2010/09/07/more-on-latex-beamer-linking-images-to-an-enlarged-version/
\newcounter{linkimagecounter}
\setcounter{linkimagecounter}{0}
%
\AtBeginDocument{%
% open file linkimage.aux for writing images' filenames to it
\newwrite\linkimageoutputstream
\immediate\openout\linkimageoutputstream=linkimage.aux
}
%
% use this command to link some content to a large picture at the end of your slides
% example:
% \linkimage{\copyrightbox{\includegraphics[height=3em]{photo2}}{Thomas Fischer}}{photo2}
% \linkimage[optional caption]{\beamerbutton{Show Photo}}{photo2}
\newcommand{\linkimage}[2]{%
\ifx\lnkmgmakro\undefined\gdef\lnkmgmakro{\errmessage{linkimage error: Something is wrong.}}\fi%
% create link anchor where link from document's end points to
\hypertarget{linkimagerefbackward\arabic{linkimagecounter}}{%
% create link pointing forward to link source in frames at document's end
\hyperlink{linkimagerefforward\arabic{linkimagecounter}}{#1}}%
\immediate\write\linkimageoutputstream{#2}%
% step counter
\addtocounter{linkimagecounter}{1}}

% call this command at the very end of your presentation (even after "Now questions, please" slide)
\newcommand{\flushlinkimages}{%
% internal counter for loop over all linked images
\newcounter{linkimagetotal}%
\setcounter{linkimagetotal}{\value{linkimagecounter}}%
\setcounter{linkimagecounter}{0}%
% close auxiliary file linkimage.aux and re-open it again for reading
\immediate\closeout\linkimageoutputstream%
\newread\linkimageinputstream%
\immediate\openin\linkimageinputstream=linkimage.aux%
% loop over all linked images ...
\whiledo{\value{linkimagecounter}<\value{linkimagetotal}}{%
% read one line (one image filename) at a time (and strip new line character at end)
\endlinechar=-1\immediate\read\linkimageinputstream to \linkimagefilename%
% create a new frame per image, center content
\only<presentation|handout:0>{%
\begin{frame}[c,plain,noframenumbering]%
\vspace*{-\titlebarheight}%
\begin{center}%
% create link pointing backward to link source in main document
\hspace*{0.5\sidebarwidth}\hyperlink{linkimagerefbackward\arabic{linkimagecounter}}{%
% create link anchor where link from main document points to
\hypertarget{linkimagerefforward\arabic{linkimagecounter}}{%
\maxsizebox*{0.9\linewidth}{0.9\paperheight}{%
\includegraphics[width=0.9\linewidth,height=0.9\paperheight,keepaspectratio]{\linkimagefilename}}%
}% hypertarget
}% hyperlink
\end{center}\end{frame}%
}%
% step counter
\addtocounter{linkimagecounter}{1}%
} % whiledo
% close file
\immediate\closein\linkimageinputstream%
}
%
\AfterEndDocument{\flushlinkimages}
%
% Provide replacement for \includegraphics.
% Currently this causes a bad horizontal spacing.
% \includelinkedgraphics[]{} is equivalent to includegraphics with additional linking.
% \linkgraphics[]{} links the image and uses it on the slide and for the zoom
% \linkgraphics[]{}[] links the image and uses 3rd argument on slide and 2nd at back.
% The first optional argument is ignored.
%
% Example:
%
% \linkgraphics[scale=0.1]{lena}
% \linkgraphics{lena}[\beamerbutton{Show Photo}]
\newcommandx{\linkgraphics}[3][1,3]{%
\ifblank{#3}{%
\only<presentation|handout:0>{\linkimage{\includegraphics[#1]{#2}}{#2}}%
\only<handout>{\includegraphics[#1]{#2}}}{%
\only<presentation|handout:0>{\linkimage{#3}{#2}}%
\only<handout>{#3}}}
%
\newcommand{\linktikz}[1]{
\global\expandafter\newsavebox\csname ngttikzlink\Roman{linkcounter}\endcsname
\hypertarget{backngttikzlink\Roman{linkcounter}}{%
\global\expandafter\sbox\csname ngttikzlink\Roman{linkcounter}\endcsname{#1}%
\hyperlink{ngttikzlink\Roman{linkcounter}}{%
\expandafter\usebox\csname ngttikzlink\Roman{linkcounter}\endcsname}}
\listcsxadd{tikzlinklist}{ngttikzlink\Roman{linkcounter}}
\stepcounter{linkcounter}
}
%
\AtEndDocument{
\ifnum\value{linkcounter} > 1
\renewcommand*{\do}[1]{
\begin{frame}[c,plain,noframenumbering]
\begin{center}
\hypertarget{#1}{\hyperlink{back#1}{\resizebox{!}{0.75\textheight}{\expandafter\usebox\csname #1\endcsname}}}
\end{center}
\end{frame}}
\dolistcsloop{tikzlinklist}
\else
\fi
}
%
% make beamer always use the frametitle template, even if frametitle is empty
\patchcmd{\endbeamer@frameslide}{%
\ifx\beamer@frametitle\@empty}{\iffalse}{}{\errmessage{failed to patch}}
