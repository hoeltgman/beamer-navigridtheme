% -*- fill-column: 100; comment-column: 81; -*-
%
% Copyright 2014-2019 Laurent Hoeltgen <contact@laurenthoeltgen.name>
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
% http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{beamerouterthemenavigrid}[2018/09/01 v0.2 Navigrid outer theme]

\RequirePackage{etoolbox}
\RequirePackage{ifthen}
\RequirePackage{bbding}
\RequirePackage{tikz}
\RequirePackage{adjustbox}
\RequirePackage{xcolor}
%
\usetikzlibrary{backgrounds}
%
% ------------------------------------------------------------------------------------------------ %
% Theme Options                                                                                    %
% ------------------------------------------------------------------------------------------------ %
%
% Define the following options:
% NumPerCol:      Number of frames per column. If total number of frames is smaller, a single
%                 column is drawn. Default is 20.
% SidebarWidth:   Width of the sidebar. Default is 1cm.
% HeadlineHeight: Height of the Frametitle. Default is 1cm.
%
\newif\ifprintnavigation\printnavigationtrue
\newif\ifprintsidebar\printsidebartrue
%
\DeclareOptionBeamer{NumPerCol}{\def\ngt@npc{#1}}
\ExecuteOptionsBeamer{NumPerCol=20}
%
\DeclareOptionBeamer{SideBarWidth}{\def\ngt@sbw{#1}}
\ExecuteOptionsBeamer{SideBarWidth=1cm}
%
\DeclareOptionBeamer{HeadLineHeight}{\def\ngt@hlh{#1}}
\ExecuteOptionsBeamer{HeadLineHeight=1cm}
%
\DeclareOptionBeamer{PrintSideBar}{\csname printsidebar#1\endcsname}
\ExecuteOptionsBeamer{PrintSideBar=true}
%
\DeclareOptionBeamer{PrintNavigation}{\csname printnavigation#1\endcsname}
\ExecuteOptionsBeamer{PrintNavigation=true}
%
\ProcessOptionsBeamer
%
% ------------------------------------------------------------------------------------------------ %
% Counters                                                                                         %
% ------------------------------------------------------------------------------------------------ %
%
% Set the maximal number of frames per column.
\newcounter{framespercolumn}
\setcounter{framespercolumn}{\ngt@npc}
%
% Define height of frame title
\newlength{\titlebarheight}
\setlength{\titlebarheight}{\ngt@hlh}
%
% Define width of the sidebar
\newlength{\sidebarwidth}
\setlength{\sidebarwidth}{\ngt@sbw}
%
% Actual number of frames per column.
\newcounter{frpercols}
\setcounter{frpercols}{0}
%
% Dummy counter for navi bar
\newcounter{framenum}
\setcounter{framenum}{0}
%
% Contains the number of coloumns
\newcounter{numcols}
\setcounter{numcols}{1}
%
% Counter needed for automatically numbering frames in header.
\newcounter{sectionframes}[section]
\setcounter{sectionframes}{0}
%
% Counter needed for automatically numbering frames in framesubtitle.
\newcounter{subsectionframes}[subsection]
\setcounter{subsectionframes}{0}
%
% ------------------------------------------------------------------------------------------------ %
% Lengths                                                                                          %
% ------------------------------------------------------------------------------------------------ %
%
% Define width of slide content (= paperwidth - sidebarwidth)
\newlength{\slidewidth}
\setlength{\slidewidth}{\paperwidth}
\addtolength{\slidewidth}{-\sidebarwidth}
%
% Define height of slide content (= paperwidth - sidebarwidth)
\newlength{\slideheight}
\setlength{\slideheight}{\textheight}
\addtolength{\slideheight}{-\titlebarheight}
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mode<presentation>
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\ifprintsidebar
\useoutertheme[right, hideallsubsections, height=\titlebarheight, width=\sidebarwidth]{sidebar}
\else
\setlength{\sidebarwidth}{0pt}
\useoutertheme[right, hideallsubsections, height=\titlebarheight, width=0pt]{sidebar}
\fi
%
% ------------------------------------------------------------------------------------------------ %
% Logo                                                                                             %
% ------------------------------------------------------------------------------------------------ %
%
\logo{%
\begin{beamercolorbox}[wd=\sidebarwidth, ht=\titlebarheight,%
      dp=0ex, sep=0pt, colsep=0pt, colsep*=0pt]{logo}
\mbox{}%
\end{beamercolorbox}%
}
%
% ------------------------------------------------------------------------------------------------ %
% Create navigation bar in right sidebar                                                           %
% ------------------------------------------------------------------------------------------------ %
%
\setbeamersize{sidebar width right=\sidebarwidth}
%
\setbeamertemplate{sidebar right}{%
% Compute number of columns
\pgfmathsetcounter{numcols}{int(ceil(\insertmainframenumber/\theframespercolumn))}%
% Compute the number of frames the belong into a single column.
\pgfmathsetcounter{frpercols}{int(ceil(\insertmainframenumber/\thenumcols))}%
% Compute the height and width of a grid cell (=box)
\pgfmathsetlengthmacro{\pgfbh}{(\paperheight-\titlebarheight)/(\thefrpercols)}%
\pgfmathsetlengthmacro{\pgfbw}{(\sidebarwidth)/(\thenumcols)}%
%
% Shift down nav bar to fit below logo.
\vspace*{\titlebarheight}%
\setcounter{framenum}{0}%
\ifprintnavigation
{\usebeamercolor{palette sidebar secondary}
\begin{tikzpicture}[%
      x=\pgfbw, y=\pgfbh, node/.style={}, tight background, show background rectangle,
      background rectangle/.style={fill=bg}, color=fg]
      \foreach \n [evaluate=\n as \y using \n+1/2] in {0,...,\numexpr\value{framespercolumn}-1}{
        \foreach \m [evaluate=\m as \z using \m+1/2] in {0,...,\numexpr\value{numcols}-1}{
          \stepcounter{framenum}
          \ifnum \value{framenum} > \insertmainframenumber
          % Do nothing we have handled all frames.
          \else
          % Current frame, highlight it with gray light background.
          \ifnum \value{framenum} = \insertframenumber
          {\usebeamercolor{palette sidebar tertiary}
          \filldraw (\z,-\y) node[fill=bg,minimum height=\pgfbh,minimum width=\pgfbw,
          inner sep=0mm,label={center:\small\bfseries\textcolor{fg}{\theframenum}}] {
            \hyperlink{NGTframe\theframenum}{\begin{tikzpicture}\node {};\end{tikzpicture}}
          };%
          }
          \else
          % Any other frame
          \filldraw (\z,-\y) node[minimum height=\pgfbh,minimum width=\pgfbw,inner sep=0mm,
          label={center:\small\bfseries\textcolor{fg}{\theframenum}}] {
            \hyperlink{NGTframe\theframenum}{\begin{tikzpicture}\node {};\end{tikzpicture}}
          };
          \fi % \value{framenum} = \insertframenumber
          \fi % \value{framenum} > \insertmainframenumber
        }%
      }%
      \draw[line width=0.1mm] (0,0) grid [xstep=\pgfbw,ystep=\pgfbh]
        (\value{numcols}+1,-\value{framespercolumn}+1);
      \draw[line width=0.1mm,xshift=-0.1mm]
        (\sidebarwidth,0) -- (\sidebarwidth,-\thefrpercols);
      \draw[line width=0.1mm,yshift=0.1mm]
        (0,-\thefrpercols) -- (\sidebarwidth,-\thefrpercols);
\end{tikzpicture}%
}% usebeamercolor
\else
\fi
}% setbeamertemplate
%
% ------------------------------------------------------------------------------------------------ %
% Frametitle                                                                                       %
% ------------------------------------------------------------------------------------------------ %
%
\setbeamertemplate{frametitle}{%
\vbox to\titlebarheight{\vfil\parbox{\linewidth}{%
\centering\usebeamerfont{frametitle}\usebeamercolor[fg]{headline}\insertframetitle}\vfil}%
\ifx\insertframesubtitle\empty\else%
\vspace*{0.75\baselineskip}%
\makebox{\parbox{\linewidth}{%
\usebeamerfont{framesubtitle}\usebeamercolor[fg]{framesubtitle}\insertframesubtitle}}\fi}
%
% Define the headline. It contains the logo and creates space to accomodate the frametitle.
\setbeamertemplate{headline}{%
\begin{beamercolorbox}[wd=\paperwidth, ht=\titlebarheight,%
dp=0ex, right, sep=0pt, colsep=0pt, colsep*=0pt]{headline}%
\hfill\vbox to\titlebarheight{\vfil\hbox{\insertlogo}\vfil}%
\end{beamercolorbox}\vspace*{-\titlebarheight}}
%
% -- Additional macros for frametitle and framesubtitle ------------------------------------------ %
\newcommand{\sectionheadframe}{%
\only<presentation>{\usebeamercolor[fg]{frametitle}\insertsection\ (\thesectionframes)}}
%
\newcommand{\subsectionheadframe}{%
\only<presentation>{\usebeamercolor[fg]{framesubtitle}\insertsubsection\ (\thesubsectionframes)}}
%
\newcommand{\tocheadframe}{%
\hspace*{-3ex}\begin{beamercolorbox}[wd=\textwidth, center, ignorebg]{frametitle}\hspace*{3ex}%
\normalsize\insertsectionnavigationhorizontal{0.95\textwidth}{%
\hskip0pt plus1filll}{\hskip0pt plus1filll}%
\end{beamercolorbox}}
%
\AtBeginEnvironment{frame}{%
\ifnum \insertframenumber < \insertmainframenumber\relax
\label{NGTframe\insertframenumber}%
\else
\fi
\stepcounter{sectionframes}%
\stepcounter{subsectionframes}}%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mode<all>
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
