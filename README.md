# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

The repository contains all the package (.sty) files. You can simply
clone this repository and place the files somewhere where your LaTeX
installation can find them.

Alternatively you can download the zipped package containing the *.ins
and *.dtx files. From the terminal run

    pdflatex navigrid.ins

to generaty the package files and

    pdflatex navigrid.dtx

to generate the package documentation. These two files are
automatically generated from this repo with makedtx.

Finally, it is planned to publish this theme on CTAN sometime in the
future, too.

* Installation
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Laurent Hoeltgen <contact@laurenthoeltgen.name>
* Other community or team contact

### License ###

This software is licensed under the Apache License version 2.0
